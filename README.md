## SETUP

- BACKEND:
``` 
	cd twelftheBackEnd
	composer install
	php artisan migrate
	php artisan db:seed
	php artisan storage:link
	php artisan serve (run server)

```


- FRONTEND;
```
	cd twelftheFrontEnd
	npm i
	ng serve (run server)
```