import { Component } from '@angular/core';
import axios from 'axios';
import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'twelfthFrontEnd';
  itemsCopy = [];
  items = [];
  view = 'active';
  actionBar = false;
  selectedIndex = 0;
  bkendURL = environment.backend_url;
  ngOnInit(){
    this.loadItems();
  }
  async loadItems(trashed=false){
    let url = !trashed ? environment.backend_url+'/api/items' : environment.backend_url+'/api/items/trashed';
    this.items = await axios.get(url)
      .then( res => res.data)
      .catch(err => console.log(err));
  }
  async delete(){
    if(confirm("Are you sure?")){
      await axios.delete(environment.backend_url+`/api/items/${this.items[this.selectedIndex]['id']}/delete`)
        .then( res => {
          this.loadItems()
          this.toggleActionBar(this.selectedIndex);
        })
    }
  }
  async download(){
    await axios.get(environment.backend_url+`/api/items/${this.items[this.selectedIndex]['id']}/download`, {responseType: 'arraybuffer'})
      .then( res => {
        let filename = (<string>this.items[this.selectedIndex]['image']['path']).split('/');
        let finalFilename = filename[filename.length - 1];
        const url = window.URL.createObjectURL(new Blob([res.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', finalFilename);
        document.body.appendChild(link);
        link.click();
      })
  }
  async restore(){
    await axios.post(environment.backend_url+`/api/items/${this.items[this.selectedIndex]['id']}/restore`)
      .then( res => {
        this.loadItems(true)
        this.toggleActionBar(this.selectedIndex);
      })
  }
  toggleActionBar(si:number){
    if(this.selectedIndex == si){
      this.actionBar = !this.actionBar;
    }else{
      this.actionBar = true;
      this.selectedIndex = si;
    }
  }

  toggleView(view:string){
    this.view = view;
    if(view === 'deleted'){
      this.loadItems(true);
    }else{
      this.loadItems();
    }
  }
}
