<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/items',  'ItemController@index');
Route::get('/items/trashed',  'ItemController@trashed');
Route::delete('/items/{id}/delete', 'ItemController@delete');
Route::post('/items/{id}/restore', 'ItemController@restore');
Route::get('/items/{id}/download', 'ItemController@download');
