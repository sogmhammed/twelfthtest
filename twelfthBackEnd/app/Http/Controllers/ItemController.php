<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use Illuminate\Http\Response;

class ItemController extends Controller
{
    public function index()
    {
        return response()->json(Item::with('image')->get());
    }
    public function trashed()
    {
        return response()->json(Item::onlyTrashed()->with('image')->get());
    }

    //delete
    public function delete($id)
    {
        $item = Item::find($id);
        $item->delete();
        return 1;
    }

    //restore
    public function restore($id)
    {
        $item = Item::withTrashed()->where('id', $id)->first();
        $item->restore();
    }

    public function download($id)
    {
        $item = Item::find($id);  
        $file = \Storage::disk('public')->path(ltrim(str_replace("storage/", "", $item->image->path), "/"));
        $content=file_get_contents($file);
        return response($content)->withHeaders([
            'Content-Type' => mime_content_type($file)
        ]);
    }


}
