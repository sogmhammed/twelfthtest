<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use App\Models\Image;
use App\Models\Item;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $files = [];
        $filesPath = storage_path() . '/app/public/images/';
        $resourceFiles = File::allFiles($filesPath);
        foreach ($resourceFiles as $rf) {
            $file_name = $rf->getFilename();
            $img = new Image();
            $img->name = $file_name;
            $img->path = '/storage/Images/'.$img->name;
            $img->save();
        }

        $items = [
            array(
                'title' => 'Partnership rationale',
                'image_id' => '3'
            ),
            array(
                'title' => 'Partnership rationale',
                'image_id' => '2'
            ),
            array(
                'title' => 'Who we are',
                'image_id' => '1'
            ),
            array(
                'title' => 'Our Clubs',
                'image_id' => '5'
            ),
            array(
                'title' => 'The Opportunity',
                'image_id' => '4'
            )
        ];

        foreach($items as $item):
            $item = Item::create($item);
        endforeach;
    }
}
